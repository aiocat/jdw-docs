# JDW Docs
JDW (Javascript Dynamic Windows) documentation source code generated with jsdoc.

## Website
Check https://aiocat.gitlab.io/jdw-docs/ for the online documentation.

## Offline Documentation
Download the source code from gitlab and open the `./web/index.html`.